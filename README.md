# Tarea No. 1
### Laboratorio - Manejo e Implementación de Archivos.

## Instrucciones
- Fork the repo. 
- Crear una rama. El nombre de la rama es: CARNET. No símbolos
ni antes ni después, solamente su número de carnet será el nombre de la rama.
- Editar el README. Deberán agregar una entrada en el área
de entregas (ver más abajo) y agregar su nombre y número de carnet.
- Crear un Merge Request. Es importante esta parte, luego de 
crear su rama y editar el README, deberán crear un pull request
para que yo pueda tomar sus cambios en cuenta.

## Notas
- El link al repositorio será publicado en Uedi.
- No habrá entregable en Uedi. El pull request es suficiente.
- Envíar Pull Request a más tardar Sábado 31 de Julio a media noche.

## Entregas
- Renato Flores, 201709244.
- Kevin Lopez, 201901016.
